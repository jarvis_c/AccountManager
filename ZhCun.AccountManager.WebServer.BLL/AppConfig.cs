﻿/**************************************************************************
创建时间:	2020/6/25
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace ZhCun.AccountManager.BLL
{
    public class AppConfig
    {
        public static readonly AppConfig THIS = new AppConfig();

        public string WX_AppId { set; get; }

        public string WX_AppSecret { set; get; }

        public string DBType { set; get; }

        public string DBConnectString { set; get; }
    }
}