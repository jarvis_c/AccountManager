﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace ZhCun.AccountManager.WebServer.WeiXin
{
    public class LoginArg
    {
        public string code { set; get; }
    }

    public class LoginRet
    { 
        /// <summary>
        /// 是否绑定了微信用户
        /// </summary>
        public bool IsBind { set; get; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { set; get; }
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { set; get; }
        /// <summary>
        /// token
        /// </summary>
        public string Token { set; get; }
        /// <summary>
        /// 过期时间戳
        /// </summary>
        public long Expires { set; get; }
    }
}
