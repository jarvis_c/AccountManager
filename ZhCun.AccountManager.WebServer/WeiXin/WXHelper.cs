﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZhCun.AccountManager.WebServer.WeiXin
{
    public class WXHelper
    {
        public static string GetUserIdUrl(string code)
        {
            string url = $"https://api.weixin.qq.com/sns/jscode2session?appid={AppConfig.THIS.WX_AppId}&secret={AppConfig.THIS.WX_AppSecret}&js_code={code}&grant_type=authorization_code";

            return url;
        }

    }
}
