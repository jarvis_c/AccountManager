﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ZhCun.AccountManager.WebServer.WeiXin;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;
using ZhCun.WebUtils;

namespace ZhCun.AccountManager.WebServer.ApiControllers
{
    //AppID(小程序ID)wx762eebeb3558a622
    //AppSecret(小程序密钥)47cafe610d94e4672e4254688749bbe2
    [Route("wx/[action]")]
    public class wxController : BaseApiController
    {
        public wxController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        IHttpClientFactory _httpClientFactory;

        [HttpPost]
        [AllowAnonymous]
        public ApiResult<LoginRet> Login(LoginArg arg)
        {
            var client = _httpClientFactory.CreateClient();
            var url = WXHelper.GetUserIdUrl(arg.code);
            var rResult = client.GetAsync(url).Result;
            if (rResult.StatusCode != System.Net.HttpStatusCode.OK)
            {
                //返回错误信息
                return ApiResult.RetErr<LoginRet>("错误的code，请求微信服务器失败");
            }
            var rJson = rResult.Content.ReadAsStringAsync().Result;
            var wxUser = JsonHelper.Deserialize<UserSession>(rJson);

            LoginRet ret = new LoginRet();
            //校验是否绑定了本地用户
            if (ret.IsBind = GetBindUser(wxUser.openid, out SessionUser user))
            {
                //ret.LoginName = user.UserId;
                ret.UserName = user.UserName;
                ret.Token = JwtHelper.CreateToken(user, TimeSpan.FromDays(1));
            }

            var bindId = Guid.NewGuid().ToString();
            MemoryCacheHelper.Set(bindId, wxUser.openid, TimeSpan.FromHours(1));
            ret.Token = bindId;
            return ApiResult.RetOK(ret);
        }

        bool GetBindUser(string wxUserId, out SessionUser user)
        {
            user = null;
            return false;
        }

        public ApiResult<LoginRet> BindUser(BindUserArg arg)
        {
            var wxUserId = MemoryCacheHelper.Get<string>(arg.BindId);
            if (wxUserId.IsEmpty())
            {
                return ApiResult.RetErr<LoginRet>("绑定失败，错误的参数Id");
            }
            SessionUser user = new SessionUser
            {
                UserId = "123",
                UserName = "张三"
            };
            LoginRet ret = new LoginRet();
            ret.UserName = user.UserName;
            ret.Token = JwtHelper.CreateToken(user, TimeSpan.FromDays(1));
            MemoryCacheHelper.Remove(arg.BindId);
            return ApiResult.RetOK(ret);
        }



        [AllowAnonymous]
        public object test()
        {
            return DateTime.Now.ToString();
        }
    }
}