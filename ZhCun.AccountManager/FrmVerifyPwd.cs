﻿/**************************************************************************
创建时间:	2020/5/17 21:28:23    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhucn.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmVerifyPwd : FrmBaseModal
    {
        public FrmVerifyPwd(Func<string, ApiResult> VerifyPwdHandle)
        {
            InitializeComponent();
            this.VerifyPwdHandle = VerifyPwdHandle;
        }

        Func<string, ApiResult> VerifyPwdHandle;

        private void FrmVerifyPwd_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ret = VerifyPwdHandle(txtPwd.Text);
            if (!ret)
            {
                ShowMessageBox("");
                DialogResult = DialogResult.No;
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}