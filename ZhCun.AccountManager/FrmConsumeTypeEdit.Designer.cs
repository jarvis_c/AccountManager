﻿namespace ZhCun.AccountManager
{
    partial class FrmConsumeTypeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editText3 = new ZhCun.Win.Controls.EditText();
            this.txtPY = new ZhCun.Win.Controls.EditText();
            this.label3 = new System.Windows.Forms.Label();
            this.editText1 = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.editCheckBox1 = new ZhCun.Win.Controls.EditCheckBox();
            this.editCheckBox2 = new ZhCun.Win.Controls.EditCheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 222);
            this.panel1.Size = new System.Drawing.Size(397, 42);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(221, 0);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(307, 0);
            // 
            // editText3
            // 
            this.editText3.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText3, "Remark");
            this.editText3.EmptyTextTip = null;
            this.editText3.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText3, true);
            this.dcm.SetIsUse(this.editText3, true);
            this.editText3.Location = new System.Drawing.Point(108, 125);
            this.editText3.MaxLength = 100;
            this.editText3.Multiline = true;
            this.editText3.Name = "editText3";
            this.editText3.RealValue = "";
            this.editText3.Size = new System.Drawing.Size(235, 66);
            this.editText3.TabIndex = 8;
            this.editText3.ValueItem = null;
            this.dcm.SetValueMember(this.editText3, "");
            // 
            // txtPY
            // 
            this.txtPY.DisplayItem = "";
            this.dcm.SetDisplayMember(this.txtPY, "TypeNamePY");
            this.txtPY.EmptyTextTip = null;
            this.txtPY.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.txtPY, true);
            this.dcm.SetIsUse(this.txtPY, true);
            this.txtPY.Location = new System.Drawing.Point(108, 56);
            this.txtPY.MaxLength = 20;
            this.txtPY.Name = "txtPY";
            this.txtPY.RealValue = "";
            this.txtPY.Size = new System.Drawing.Size(235, 26);
            this.txtPY.TabIndex = 5;
            this.txtPY.ValueItem = null;
            this.dcm.SetValueMember(this.txtPY, "");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "备注：";
            // 
            // editText1
            // 
            this.editText1.CodePYTextBox = this.txtPY;
            this.editText1.DisplayItem = "";
            this.dcm.SetDisplayMember(this.editText1, "TypeName");
            this.editText1.EmptyTextTip = null;
            this.editText1.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dcm.SetEnterIsTab(this.editText1, true);
            this.dcm.SetIsUse(this.editText1, true);
            this.editText1.Location = new System.Drawing.Point(108, 21);
            this.editText1.MaxLength = 20;
            this.editText1.Name = "editText1";
            this.editText1.RealValue = "";
            this.editText1.Size = new System.Drawing.Size(235, 26);
            this.editText1.TabIndex = 4;
            this.editText1.ValueItem = null;
            this.dcm.SetValueMember(this.editText1, "Id");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "助记码：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "类型名称：";
            // 
            // editCheckBox1
            // 
            this.editCheckBox1.AutoSize = true;
            this.editCheckBox1.DisplayItem = false;
            this.dcm.SetDisplayMember(this.editCheckBox1, "IsBalanceOut");
            this.dcm.SetEnterIsTab(this.editCheckBox1, true);
            this.dcm.SetIsUse(this.editCheckBox1, true);
            this.editCheckBox1.Location = new System.Drawing.Point(112, 92);
            this.editCheckBox1.Name = "editCheckBox1";
            this.editCheckBox1.Size = new System.Drawing.Size(84, 24);
            this.editCheckBox1.TabIndex = 6;
            this.editCheckBox1.Text = "资金支出";
            this.editCheckBox1.UseVisualStyleBackColor = true;
            this.editCheckBox1.ValueItem = null;
            this.dcm.SetValueMember(this.editCheckBox1, "");
            // 
            // editCheckBox2
            // 
            this.editCheckBox2.AutoSize = true;
            this.editCheckBox2.DisplayItem = false;
            this.dcm.SetDisplayMember(this.editCheckBox2, "IsBalanceIn");
            this.dcm.SetEnterIsTab(this.editCheckBox2, true);
            this.dcm.SetIsUse(this.editCheckBox2, true);
            this.editCheckBox2.Location = new System.Drawing.Point(239, 92);
            this.editCheckBox2.Name = "editCheckBox2";
            this.editCheckBox2.Size = new System.Drawing.Size(84, 24);
            this.editCheckBox2.TabIndex = 7;
            this.editCheckBox2.Text = "资金收入";
            this.editCheckBox2.UseVisualStyleBackColor = true;
            this.editCheckBox2.ValueItem = null;
            this.dcm.SetValueMember(this.editCheckBox2, "");
            // 
            // FrmConsumeTypeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 264);
            this.Controls.Add(this.editCheckBox2);
            this.Controls.Add(this.editCheckBox1);
            this.Controls.Add(this.editText3);
            this.Controls.Add(this.txtPY);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.editText1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmConsumeTypeEdit";
            this.Text = "账目类型";
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.editText1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtPY, 0);
            this.Controls.SetChildIndex(this.editText3, 0);
            this.Controls.SetChildIndex(this.editCheckBox1, 0);
            this.Controls.SetChildIndex(this.editCheckBox2, 0);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Win.Controls.EditText editText3;
        private Win.Controls.EditText txtPY;
        private System.Windows.Forms.Label label3;
        private Win.Controls.EditText editText1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Win.Controls.EditCheckBox editCheckBox1;
        private Win.Controls.EditCheckBox editCheckBox2;
    }
}