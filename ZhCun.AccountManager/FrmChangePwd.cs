﻿/**************************************************************************
创建时间:	2020/5/16 22:30:16    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：修改密码
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.Utils;
using ZhCun.Win;

namespace ZhCun.AccountManager
{
    public partial class FrmChangePwd : FrmBaseModal
    {
        public FrmChangePwd(Func<string, string, string, ApiResult> ChangePwdHandle)
        {
            InitializeComponent();
            this.ChangePwdHandle = ChangePwdHandle;
        }

        Func<string, string, string, ApiResult> ChangePwdHandle;

        private void FrmChangePwd_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var ret = ChangePwdHandle(txtOldPwd.Text, txtNewPwd1.Text, txtNewPwd2.Text);
            if (!ret)
            {
                ShowMessage(ret.msg);
                return;
            }
            this.DialogResult = DialogResult.OK;
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtOldPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNewPwd1.Focus();
            }
        }

        private void txtNewPwd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNewPwd2.Focus();
            }
        }

        private void txtNewPwd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOK.Focus();
            }
        }
    }
}