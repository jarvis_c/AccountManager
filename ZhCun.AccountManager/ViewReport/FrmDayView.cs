﻿/**************************************************************************
创建时间:	2020/5/19 17:05:21    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：日统计报表
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZhCun.AccountManager.BLL;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.Win;
using ZhCun.Win.Controls;

namespace ZhCun.AccountManager.ViewReport
{
    public partial class FrmDayView : FrmBase
    {
        public FrmDayView()
        {
            InitializeComponent();
        }

        readonly ViewDayBLL BLLObj = new ViewDayBLL();

        readonly ArgSumSearch _Arg = new ArgSumSearch();

        private void FrmDayView_Load(object sender, EventArgs e)
        {
            tsDateType.SelectedIndex = 0;
            tsStartTime.Value = DateTime.Now.AddMonths(-1);
            tsEndTime.Value = DateTime.Now;
        }
                
        private void tsBtnRefresh_Click(object sender, EventArgs e)
        {
            _Arg.StartTime = tsStartTime.Value;
            _Arg.EndTime = tsEndTime.Value;
            _Arg.DateType = tsDateType.SelectedIndex;
            var data = BLLObj.GetViewData(_Arg);
            dgv.DataSource = data;
            tslbAmountOut.Text = Math.Abs(_Arg.SumAmountOut).ToString();
            tslbAmountIn.Text = _Arg.SumAmountIn.ToString();
            tslbAmount.Text = (_Arg.SumAmountIn + _Arg.SumAmountOut).ToString();
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
