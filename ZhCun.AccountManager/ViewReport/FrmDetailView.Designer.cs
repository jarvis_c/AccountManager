﻿namespace ZhCun.AccountManager.ViewReport
{
    partial class FrmDetailView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsiStartTime = new ZhCun.Win.Controls.ToolStripDateTimePicker();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsiEndTime = new ZhCun.Win.Controls.ToolStripDateTimePicker();
            this.tsDateType = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.tsTxtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtnSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsAdSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ucPage = new ZhCun.Win.Controls.UcPageNav();
            this.dgv = new ZhCun.Win.Controls.GridView();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountOut = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmountIn = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripTop.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripTop
            // 
            this.toolStripTop.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsiStartTime,
            this.toolStripLabel2,
            this.tsiEndTime,
            this.tsDateType,
            this.toolStripSeparator2,
            this.tsbtnExit,
            this.tsTxtSearch,
            this.tsBtnSearch,
            this.toolStripSeparator3,
            this.tsAdSearch,
            this.toolStripSeparator1});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(987, 39);
            this.toolStripTop.TabIndex = 35;
            this.toolStripTop.Text = "工具栏";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(51, 36);
            this.toolStripLabel1.Text = "日期：";
            // 
            // tsiStartTime
            // 
            this.tsiStartTime.AutoSize = false;
            this.tsiStartTime.BackColor = System.Drawing.Color.Transparent;
            this.tsiStartTime.Checked = true;
            this.tsiStartTime.CustomFormat = null;
            this.tsiStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.tsiStartTime.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.tsiStartTime.Name = "tsiStartTime";
            this.tsiStartTime.ShowCheckBox = false;
            this.tsiStartTime.Size = new System.Drawing.Size(150, 32);
            this.tsiStartTime.Text = "tsiStartTime";
            this.tsiStartTime.Value = new System.DateTime(2020, 5, 23, 11, 36, 17, 547);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(15, 36);
            this.toolStripLabel2.Text = "-";
            // 
            // tsiEndTime
            // 
            this.tsiEndTime.AutoSize = false;
            this.tsiEndTime.BackColor = System.Drawing.Color.Transparent;
            this.tsiEndTime.Checked = true;
            this.tsiEndTime.CustomFormat = null;
            this.tsiEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.tsiEndTime.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.tsiEndTime.Name = "tsiEndTime";
            this.tsiEndTime.ShowCheckBox = false;
            this.tsiEndTime.Size = new System.Drawing.Size(150, 32);
            this.tsiEndTime.Text = "toolStripDateTimePicker1";
            this.tsiEndTime.Value = new System.DateTime(2020, 5, 23, 11, 36, 35, 958);
            // 
            // tsDateType
            // 
            this.tsDateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tsDateType.Items.AddRange(new object[] {
            "记录时间",
            "发生时间"});
            this.tsDateType.Name = "tsDateType";
            this.tsDateType.Size = new System.Drawing.Size(100, 39);
            this.tsDateType.ToolTipText = "选择是按记录时间筛选记录,还是发生时间筛选";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbtnExit.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnExit.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnExit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnExit.Image = global::ZhCun.AccountManager.Properties.Resources.exit;
            this.tsbtnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(73, 36);
            this.tsbtnExit.Text = "退出";
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // tsTxtSearch
            // 
            this.tsTxtSearch.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.tsTxtSearch.Margin = new System.Windows.Forms.Padding(5);
            this.tsTxtSearch.Name = "tsTxtSearch";
            this.tsTxtSearch.Padding = new System.Windows.Forms.Padding(2);
            this.tsTxtSearch.Size = new System.Drawing.Size(108, 29);
            this.tsTxtSearch.ToolTipText = "输入搜索内容回车";
            this.tsTxtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tsTxtSearch_KeyDown);
            // 
            // tsBtnSearch
            // 
            this.tsBtnSearch.Checked = true;
            this.tsBtnSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSearch.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnSearch.Name = "tsBtnSearch";
            this.tsBtnSearch.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnSearch.Size = new System.Drawing.Size(79, 36);
            this.tsBtnSearch.Text = "检索(&S)";
            this.tsBtnSearch.Click += new System.EventHandler(this.tsBtnSearch_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // tsAdSearch
            // 
            this.tsAdSearch.BackColor = System.Drawing.Color.Transparent;
            this.tsAdSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsAdSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsAdSearch.Image = global::ZhCun.AccountManager.Properties.Resources.search;
            this.tsAdSearch.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsAdSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsAdSearch.Name = "tsAdSearch";
            this.tsAdSearch.Size = new System.Drawing.Size(101, 36);
            this.tsAdSearch.Text = "高级搜索";
            this.tsAdSearch.Click += new System.EventHandler(this.tsAdSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel2,
            this.tslbAmountOut,
            this.toolStripStatusLabel4,
            this.tslbAmountIn,
            this.toolStripStatusLabel6,
            this.tslbAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 516);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(987, 25);
            this.statusStrip1.TabIndex = 36;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ucPage
            // 
            this.ucPage.AutoSize = true;
            this.ucPage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPage.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucPage.Location = new System.Drawing.Point(0, 474);
            this.ucPage.Name = "ucPage";
            this.ucPage.Size = new System.Drawing.Size(987, 42);
            this.ucPage.TabIndex = 38;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.DisplayRowNo = true;
            this.dgv.DisplayRowNoStyle = ZhCun.Win.Controls.GridView.RowNoStyle.CustomColumn;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 39);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(987, 435);
            this.dgv.TabIndex = 39;
            this.dgv.UseControlStyle = true;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(51, 20);
            this.toolStripStatusLabel2.Text = "支出：";
            // 
            // tslbAmountOut
            // 
            this.tslbAmountOut.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountOut.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountOut.Name = "tslbAmountOut";
            this.tslbAmountOut.Size = new System.Drawing.Size(36, 20);
            this.tslbAmountOut.Text = "0.00";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(51, 20);
            this.toolStripStatusLabel4.Text = "收入：";
            // 
            // tslbAmountIn
            // 
            this.tslbAmountIn.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmountIn.ForeColor = System.Drawing.Color.Red;
            this.tslbAmountIn.Name = "tslbAmountIn";
            this.tslbAmountIn.Size = new System.Drawing.Size(36, 20);
            this.tslbAmountIn.Text = "0.00";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(65, 20);
            this.toolStripStatusLabel6.Text = "收支差：";
            // 
            // tslbAmount
            // 
            this.tslbAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbAmount.Name = "tslbAmount";
            this.tslbAmount.Size = new System.Drawing.Size(36, 20);
            this.tslbAmount.Text = "0.00";
            // 
            // FrmDetailView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 541);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.ucPage);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStripTop);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmDetailView";
            this.Text = "综合明细查询";
            this.Load += new System.EventHandler(this.FrmDetailView_Load);
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.ToolStrip toolStripTop;
        public System.Windows.Forms.ToolStripButton tsAdSearch;
        protected System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        public System.Windows.Forms.ToolStripButton tsbtnExit;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripButton tsBtnSearch;
        private Win.Controls.UcPageNav ucPage;
        private Win.Controls.GridView dgv;
        private System.Windows.Forms.ToolStripComboBox tsDateType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox tsTxtSearch;
        private Win.Controls.ToolStripDateTimePicker tsiStartTime;
        private Win.Controls.ToolStripDateTimePicker tsiEndTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountOut;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmountIn;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel tslbAmount;
    }
}