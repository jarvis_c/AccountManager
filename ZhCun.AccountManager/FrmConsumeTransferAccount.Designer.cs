﻿namespace ZhCun.AccountManager
{
    partial class FrmConsumeTransferAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBalanceOut = new ZhCun.Win.Controls.EditNumber();
            this.txtAmount = new ZhCun.Win.Controls.EditNumber();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAccountOut = new ZhCun.Win.Controls.EditText();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAccountIn = new ZhCun.Win.Controls.EditText();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBalanceIn = new ZhCun.Win.Controls.EditNumber();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtBalanceOut
            // 
            this.txtBalanceOut.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceOut.EmptyTextTip = null;
            this.txtBalanceOut.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtBalanceOut.FormatString = "#,##0.00";
            this.txtBalanceOut.Location = new System.Drawing.Point(155, 76);
            this.txtBalanceOut.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtBalanceOut.Name = "txtBalanceOut";
            this.txtBalanceOut.PrefixSign = "￥";
            this.txtBalanceOut.ReadOnly = true;
            this.txtBalanceOut.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceOut.Size = new System.Drawing.Size(230, 32);
            this.txtBalanceOut.TabIndex = 12;
            this.txtBalanceOut.Text = "￥0.00";
            this.txtBalanceOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBalanceOut.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceOut.ValueItem = null;
            // 
            // txtAmount
            // 
            this.txtAmount.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtAmount.EmptyTextTip = null;
            this.txtAmount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtAmount.FormatString = "#,##0.00";
            this.txtAmount.Location = new System.Drawing.Point(155, 209);
            this.txtAmount.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PrefixSign = "￥";
            this.txtAmount.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtAmount.Size = new System.Drawing.Size(230, 32);
            this.txtAmount.TabIndex = 3;
            this.txtAmount.Text = "￥0.00";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtAmount.ValueItem = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(51, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "转出余额：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(51, 212);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "转账金额：";
            // 
            // txtAccountOut
            // 
            this.txtAccountOut.DisplayItem = "";
            this.txtAccountOut.EmptyTextTip = "输入助记码检索";
            this.txtAccountOut.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtAccountOut.Location = new System.Drawing.Point(155, 31);
            this.txtAccountOut.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtAccountOut.Name = "txtAccountOut";
            this.txtAccountOut.RealValue = "";
            this.txtAccountOut.Size = new System.Drawing.Size(230, 32);
            this.txtAccountOut.TabIndex = 1;
            this.txtAccountOut.ValueItem = null;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(51, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "转出账户：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(51, 123);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "转入账户：";
            // 
            // txtAccountIn
            // 
            this.txtAccountIn.DisplayItem = "";
            this.txtAccountIn.EmptyTextTip = "输入助记码检索";
            this.txtAccountIn.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtAccountIn.Location = new System.Drawing.Point(155, 120);
            this.txtAccountIn.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtAccountIn.Name = "txtAccountIn";
            this.txtAccountIn.RealValue = "";
            this.txtAccountIn.Size = new System.Drawing.Size(230, 32);
            this.txtAccountIn.TabIndex = 2;
            this.txtAccountIn.ValueItem = null;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(51, 168);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "转入余额：";
            // 
            // txtBalanceIn
            // 
            this.txtBalanceIn.DisplayItem = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceIn.EmptyTextTip = null;
            this.txtBalanceIn.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.txtBalanceIn.FormatString = "#,##0.00";
            this.txtBalanceIn.Location = new System.Drawing.Point(155, 165);
            this.txtBalanceIn.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtBalanceIn.Name = "txtBalanceIn";
            this.txtBalanceIn.PrefixSign = "￥";
            this.txtBalanceIn.ReadOnly = true;
            this.txtBalanceIn.RealValue = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceIn.Size = new System.Drawing.Size(230, 32);
            this.txtBalanceIn.TabIndex = 12;
            this.txtBalanceIn.Text = "￥0.00";
            this.txtBalanceIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBalanceIn.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtBalanceIn.ValueItem = null;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(327, 261);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 36);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(78, 262);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 36);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FrmConsumeTransferAccount
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(495, 323);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtBalanceIn);
            this.Controls.Add(this.txtBalanceOut);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAccountIn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAccountOut);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "FrmConsumeTransferAccount";
            this.Text = "转账";
            this.Load += new System.EventHandler(this.FrmConsumeTransferAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Win.Controls.EditNumber txtBalanceOut;
        private Win.Controls.EditNumber txtAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private Win.Controls.EditText txtAccountOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Win.Controls.EditText txtAccountIn;
        private System.Windows.Forms.Label label4;
        private Win.Controls.EditNumber txtBalanceIn;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
    }
}