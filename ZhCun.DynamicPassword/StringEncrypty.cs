﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.DynamicPassword
{
    public class StringEncrypty : IStringDynamicEncrypty
    {
        static StringEncrypty()
        {
            var disk = AtapiDevice.GetHddInfo(1);
            DiskSerialNumber = disk.SerialNumber;
        }

        private static readonly string DiskSerialNumber;

        public string GetCiphertext(string key, string plaintext)
        {
            key = $"{DiskSerialNumber}@{key}";
            return DEncryptByDes.Encrypt(plaintext, key);
        }

        public string GetPlaintext(string key, string ciphertext)
        {
            key = $"{DiskSerialNumber}@{key}";
            return DEncryptByDes.Decrypt(ciphertext, key);
        }
    }
}
