﻿/**************************************************************************
创建时间:	2020/5/19 15:56:29    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.DbCore.Cores;
using ZhCun.DbCore.Entitys;
using ZhCun.Utils;

namespace ZhCun.AccountManager.BLL
{
    /// <summary>
    /// 增删查改的BLL实现
    /// </summary>
    public abstract class BLLBaseCrud<TModel> : BLLBaseCrud<TModel, TModel>
       where TModel : EntityBase, new()
    { }

    /// <summary>
    /// 增删查改的BLL实现(仅实现编辑与新增试题相同的实体类，且实体对象为EntityBase)
    /// 非数据库实体类，可使用该实现
    /// </summary>
    /// <typeparam name="TSaveModel">新增实体类型</typeparam>
    /// <typeparam name="TViewModel">查询实体类型</typeparam>
    public abstract class BLLBaseCrud<TSaveModel, TViewModel> : BLLBase
        where TSaveModel : EntityBase, new()
        where TViewModel : EntityBase, new()
    {
        /// <summary>
        /// 验证输入，并返回错误的属性名，属性名可定位控件焦点
        /// </summary>
        protected abstract ApiResult<string> VerifyModel(TSaveModel model, bool isAdd);
        /// <summary>
        /// 新增保存
        /// </summary>
        public virtual ApiResult<string> Add(TSaveModel model)
        {
            var ret = VerifyModel(model, true);
            if (ret)
            {
                DB.Insert(model);
            }
            return ret;
        }
        /// <summary>
        /// 编辑保存
        /// </summary>
        public virtual ApiResult<string> Edit(TSaveModel model)
        {
            var ret = VerifyModel(model, false);
            if (ret)
            {
                DB.Update(model);
            }
            return ret;
        }
        /// <summary>
        /// 删除
        /// </summary>
        public virtual ApiResult Del(TViewModel model)
        {
            var ret = DB.Delete(model);

            if (ret.RowCount == 1)
            {
                return RetOK("删除成功");
            }
            return RetErr("删除失败");
        }
        /// <summary>
        /// 设置查询条件
        /// </summary>
        protected abstract void SetQuery(QueryCondition<TViewModel> query, string searchVal);
        /// <summary>
        /// 获取数据
        /// </summary>
        public virtual DataTable GetData(int pageNo, int pageSize, string searchVal, out int rowCount)
        {
            var query = DB.CreateQuery<TViewModel>();
            query.PageNo = pageNo;
            query.PageSize = pageSize;
            SetQuery(query, searchVal);
            var ret = DB.Query<TViewModel>(query, ADJoinSql);
            var dt = ret.ToDataTable();
            rowCount = query.Total;
            return dt;
        }
    }
}