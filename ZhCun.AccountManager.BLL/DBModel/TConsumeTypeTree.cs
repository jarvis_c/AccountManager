using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class TConsumeTypeTree : EntityBase
	{
		private System.String _Id;
		/// <summary>
		/// Id
		/// </summary>
		[Entity(ColumnName = CNId, IsPrimaryKey = true, IsNotNull = true)]
		public System.String Id
		{
			get { return _Id; }
			set
			{
				_Id = value;
				base.SetFieldChanged(CNId) ;
			}
		}

		private System.String _PId;
		/// <summary>
		/// PId
		/// </summary>
		[Entity(ColumnName = CNPId, IsNotNull = true)]
		public System.String PId
		{
			get { return _PId; }
			set
			{
				_PId = value;
				base.SetFieldChanged(CNPId) ;
			}
		}

		private System.String _TreeName;
		/// <summary>
		/// TreeName
		/// </summary>
		[Entity(ColumnName = CNTreeName, IsNotNull = true)]
		public System.String TreeName
		{
			get { return _TreeName; }
			set
			{
				_TreeName = value;
				base.SetFieldChanged(CNTreeName) ;
			}
		}

		private System.String _ConsumeTypeId;
		/// <summary>
		/// ConsumeTypeId
		/// </summary>
		[Entity(ColumnName = CNConsumeTypeId)]
		public System.String ConsumeTypeId
		{
			get { return _ConsumeTypeId; }
			set
			{
				_ConsumeTypeId = value;
				base.SetFieldChanged(CNConsumeTypeId) ;
			}
		}

		private System.String _ConsumeTypeName;
		/// <summary>
		/// ConsumeTypeName
		/// </summary>
		[Entity(ColumnName = CNConsumeTypeName)]
		public System.String ConsumeTypeName
		{
			get { return _ConsumeTypeName; }
			set
			{
				_ConsumeTypeName = value;
				base.SetFieldChanged(CNConsumeTypeName) ;
			}
		}

		private System.DateTime _AddTime;
		/// <summary>
		/// AddTime
		/// </summary>
		[Entity(ColumnName = CNAddTime, IsNotNull = true)]
		public System.DateTime AddTime
		{
			get { return _AddTime; }
			set
			{
				_AddTime = value;
				base.SetFieldChanged(CNAddTime) ;
			}
		}

		private System.String _AddUserId;
		/// <summary>
		/// AddUserId
		/// </summary>
		[Entity(ColumnName = CNAddUserId)]
		public System.String AddUserId
		{
			get { return _AddUserId; }
			set
			{
				_AddUserId = value;
				base.SetFieldChanged(CNAddUserId) ;
			}
		}

		private System.String _AddHost;
		/// <summary>
		/// AddHost
		/// </summary>
		[Entity(ColumnName = CNAddHost)]
		public System.String AddHost
		{
			get { return _AddHost; }
			set
			{
				_AddHost = value;
				base.SetFieldChanged(CNAddHost) ;
			}
		}

		private System.DateTime? _LastTime;
		/// <summary>
		/// LastTime
		/// </summary>
		[Entity(ColumnName = CNLastTime)]
		public System.DateTime? LastTime
		{
			get { return _LastTime; }
			set
			{
				_LastTime = value;
				base.SetFieldChanged(CNLastTime) ;
			}
		}

		private System.String _LastHost;
		/// <summary>
		/// LastHost
		/// </summary>
		[Entity(ColumnName = CNLastHost)]
		public System.String LastHost
		{
			get { return _LastHost; }
			set
			{
				_LastHost = value;
				base.SetFieldChanged(CNLastHost) ;
			}
		}

		private System.String _LastUserId;
		/// <summary>
		/// LastUserId
		/// </summary>
		[Entity(ColumnName = CNLastUserId)]
		public System.String LastUserId
		{
			get { return _LastUserId; }
			set
			{
				_LastUserId = value;
				base.SetFieldChanged(CNLastUserId) ;
			}
		}

		#region 字段名的定义
		public const string CNId = "Id";
		public const string CNPId = "PId";
		public const string CNTreeName = "TreeName";
		public const string CNConsumeTypeId = "ConsumeTypeId";
		public const string CNConsumeTypeName = "ConsumeTypeName";
		public const string CNAddTime = "AddTime";
		public const string CNAddUserId = "AddUserId";
		public const string CNAddHost = "AddHost";
		public const string CNLastTime = "LastTime";
		public const string CNLastHost = "LastHost";
		public const string CNLastUserId = "LastUserId";
		#endregion

	}
}
