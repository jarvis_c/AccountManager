using System;
using ZhCun.DbCore.Entitys;

namespace ZhCun.AccountManager.DBModel
{
	public partial class V_Random : EntityBase
	{
		private System.Double? _RndNumber;
		/// <summary>
		/// RndNumber
		/// </summary>
		[Entity(ColumnName = CNRndNumber, IsPrimaryKey = true)]
		public System.Double? RndNumber
		{
			get { return _RndNumber; }
			set
			{
				_RndNumber = value;
				base.SetFieldChanged(CNRndNumber) ;
			}
		}

		#region 字段名的定义
		public const string CNRndNumber = "RndNumber";
		#endregion

	}
}
