﻿/**************************************************************************
创建时间:	2020/5/23 22:58:07    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;

namespace ZhCun.AccountManager.BLL
{
    public class ViewDayBLL : ViewDateSumBase
    {
        public DataTable GetViewData(ArgSumSearch arg)
        {
            var detail = GetViewDetail(arg);

            decimal inAmount, outAmount;

            DataTable dt = new DataTable();
            dt.Columns.Add("SumDate", typeof(DateTime));
            dt.Columns.Add("AmountIn", typeof(decimal));
            dt.Columns.Add("AmountOut",typeof(decimal));
            dt.Columns.Add("Amount",typeof(decimal));
            dt.Columns.Add("UseCount", typeof(int));

            DateTime date = arg.StartTime.Date;
            while (date <= arg.EndTime)
            {
                var dr = dt.NewRow();
                
                dr["SumDate"] = date;
                var groupData = arg.DateType == 1
                   ? detail.Where(s => s.ConsumeDate >= date.Date && s.ConsumeDate < date.AddDays(1))
                   : detail.Where(s => s.AddTime.Date >= date.Date && s.AddTime < date.AddDays(1));

                outAmount = groupData.Where(s => s.Amount < 0).Sum(s => s.Amount);
                inAmount = groupData.Where(s => s.Amount > 0).Sum(s => s.Amount);
                dr["AmountIn"] = inAmount;
                dr["AmountOut"] = outAmount;
                dr["Amount"] = inAmount + outAmount;
                dr["UseCount"] = groupData.Count();

                dt.Rows.Add(dr);
                date = date.AddDays(1);
            }

            return dt;
        }
    }
}
