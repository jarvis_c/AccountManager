﻿/**************************************************************************
创建时间:	2020/5/19 16:21:01    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore.Cores;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class ConsumeTypeBLL : BLLBaseCrud<TConsumeType>
    {
        public override ApiResult Del(TConsumeType model)
        {
            if (model.LastTime != null)
            {
                return RetErr("类型已经使用不允许删除");
            }
            return base.Del(model);
        }

        protected override void SetQuery(QueryCondition<TConsumeType> query, string searchVal)
        {
            query.WhereAnd(s => s.IsType == true);
            if (!searchVal.IsEmpty())
            {
                string likeStr = $"%{searchVal}%";

                query.WhereAnd(s =>
                    s.WEx_Like(s.TypeName, likeStr) ||
                    s.WEx_Like(s.TypeNamePY, likeStr)
                    );
            }
            query.OrderBy(s => s.AddTime);
        }

        protected override ApiResult<string> VerifyModel(TConsumeType model, bool isAdd)
        {
            if (model.TypeName.IsEmpty())
            {
                return RetErr(nameof(model.TypeName), "类型名称不能为空");
            }
            if (DB.QueryExist<TConsumeType>(s => s.TypeName == model.TypeName && s.Id != model.Id))
            {
                return RetErr(nameof(model.TypeName), "类型名称已存在");
            }
            if (isAdd)
            {
                model.IsType = true;
            }
            return RetOK<string>(string.Empty, isAdd ? "新增类型成功" : "更新类型成功");
        }

    }
}
