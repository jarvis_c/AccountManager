﻿/**************************************************************************
创建时间:	2020/5/17 20:45:26    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class MainBLL : BLLBase
    {
        public ApiResult ChangePwd(string oldPwd, string newPwd1, string newPwd2)
        {
            if (StringHelper.IsNullOrWhiteSpace(oldPwd, newPwd1, newPwd2))
            {
                return RetErr("密码不能为空");
            }
            if (newPwd1 != newPwd2)
            {
                return RetErr("两次新密码不一致");
            }
            TUser user = new TUser
            {
                Id = CurrUser.Id,
                LoginPwd = newPwd1
            };
            var ret = DB.Update(user, s => s.Id == user.Id && s.LoginPwd == oldPwd);
            if (ret.RowCount == 1)
            {
                return RetOK("修改密码成功");
            }
            else
            {
                return RetOK("原密码错误");
            }
        }

        public ApiResult VerifyPwd(string pwd)
        {
            if (DB.QueryExist<TUser>(s => s.Id == CurrUser.Id && s.LoginPwd == pwd))
            {
                return RetOK("验证密码成功");
            }
            return RetErr("验证密码错误");
        }
    }
}