﻿/**************************************************************************
创建时间:	2020/5/21 14:45:11    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL
{
    public class DateRange
    {
        public DateRangeType DateType { set; get; }

        public DateTime? DateValue
        {
            get
            {
                if (DateType == DateRangeType.本周)
                {
                    int weekNo = (int)DateTime.Now.DayOfWeek;
                    if (weekNo == 0) weekNo = 7;  //周一开始
                    return DateTime.Now.AddDays(weekNo * -1 + 1).Date;
                }
                else if (DateType == DateRangeType.本月)
                {
                    return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                }
                else if (DateType == DateRangeType.本年)
                {
                    return new DateTime(DateTime.Now.Year, 1, 1);
                }
                return DateTime.Now.AddDays((int)DateType * -1).Date;
            }
        }

        public override string ToString()
        {
            return DateType.ToString();
        }
    }

    public enum DateRangeType : int
    {
        本周 = -1,
        本月 = -2,        
        本年 = -3,
        一周内 = 7,
        一月内 = 30,
        半年内 = 180,
        一年内 = 365,
        全部 = 10000
    }
}