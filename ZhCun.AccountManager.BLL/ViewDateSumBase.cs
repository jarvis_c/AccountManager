﻿/**************************************************************************
创建时间:	2020/5/27 22:10:50    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：按日期搜索统计的基类
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.BLL.VM;
using ZhCun.AccountManager.DBModel;

namespace ZhCun.AccountManager.BLL
{
    /// <summary>
    /// 
    /// </summary>
    public class ViewDateSumBase : BLLBase
    {
        protected List<TConsume> GetViewDetail(ArgSumSearch arg)
        {
            var query = DB.CreateQuery<TConsume>();
            if (arg.DateType == 1)
                query.WhereAnd(s => s.ConsumeDate >= arg.StartTime && s.ConsumeDate < arg.EndTime);
            else
                query.WhereAnd(s => s.AddTime >= arg.StartTime && s.AddTime < arg.EndTime);

            List<TConsume> rData = DB.Query<TConsume>(query).ToList(true);
            arg.SumAmountOut = rData.Where(s => s.Amount < 0).Sum(s => s.Amount);
            arg.SumAmountIn = rData.Where(s => s.Amount > 0).Sum(s => s.Amount);

            return rData;
        }
    }
}
