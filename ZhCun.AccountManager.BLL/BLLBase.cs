﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore;
using ZhCun.DbCore.BuildSQLText;
using ZhCun.DbCore.Cores;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class BLLBase
    {
        static TUser _CurrUser;

        /// <summary>
        /// 当前登陆用户
        /// </summary>
        public TUser CurrUser
        {
            get { return _CurrUser; }
            protected set
            {
                _CurrUser = value;
            }
        }

        internal DBHelper DB { get; }

        public BLLBase()
        {
            DB = new DBHelper(CurrUser?.Id);
        }

        #region 返回结果的创建及定义

        /// <summary>
        /// 成功结果，可指定成功消息
        /// </summary>
        protected ApiResult RetOK(string msg = null)
        {
            return ApiResult.RetOK(msg);
        }
        /// <summary>
        /// 成功结果，并返回实体对象的结果
        /// </summary>
        protected ApiResult<T> RetOK<T>(T data, string msg = "")
        {
            return ApiResult.RetOK(data, msg);
        }
        /// <summary>
        /// 成功结果，返回DataTable 结果集，count 为分页的全部记录数
        /// </summary>
        protected ApiResultDataTable RetOK(DataTable data, int count, string msg = "")
        {
            return ApiResult.RetOK(data, count, msg);
        }
        /// <summary>
        /// 返回空的DataTable 数据
        /// </summary>
        protected ApiResultDataTable RetNullData(string msg)
        {
            return new ApiResultDataTable() { code = ApiResult.CODE_VERIFY_FAIL, msg = msg };
        }
        /// <summary>
        /// 返回一个实体集合结果
        /// </summary>
        protected ApiResultList<T> RetOK<T>(List<T> data, int count, string msg = "")
        {
            return ApiResult.RetOK(data, count, msg);
        }
        /// <summary>
        /// 返回空的 List 数据
        /// </summary>
        protected ApiResultList<T> RetNullData<T>(string msg)
        {
            return new ApiResultList<T>() { code = ApiResult.CODE_VERIFY_FAIL, msg = msg };
        }
        /// <summary>
        /// 正常逻辑错误
        /// </summary>
        protected ApiResult RetErr(string msg)
        {
            return ApiResult.RetErr(msg);
        }
        /// <summary>
        /// 返回数据对象的错误结果
        /// </summary>
        protected ApiResult<T> RetErr<T>(string msg)
        {
            return ApiResult.RetErr<T>(msg);
        }
        /// <summary>
        /// 返回验证错误,data 表示属性名称
        /// </summary>
        protected ApiResult<T> RetErr<T>(T t, string msg)
        {
            return new ApiResult<T>() { data = t, code = ApiResult.CODE_VERIFY_FAIL, msg = msg };
        }

        #endregion

        ISqlBuilder _ADJoinSql;
        /// <summary>
        /// 用于高级搜索 合并sql对象,
        /// </summary>
        public ISqlBuilder ADJoinSql
        {
            get
            {
                if (_ADJoinSql == null)
                {
                    _ADJoinSql = DB.CreateSqlBuilder("AD");
                }
                return _ADJoinSql;
            }
        }
        /// <summary>
        /// 清除高级搜索的SqlBuiler
        /// </summary>
        public void ClearADSql()
        {
            _ADJoinSql?.ClearResult();
        }
        /// <summary>
        /// 创建一个自定义Guid
        /// </summary>
        public string NewId()
        {
            return GuidHelper.NewId();
        }
    }
}