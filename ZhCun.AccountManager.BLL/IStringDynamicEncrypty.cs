﻿/**************************************************************************
创建时间:	2020/5/28 17:30:38    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述： 用于实现动态字符串加解密的接口定义
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.Utils
{
    /// <summary>
    /// 用于字符串动态加解密的定义
    /// </summary>
    public interface IStringDynamicEncrypty
    {
        /// <summary>
        /// 明问转密文
        /// </summary>
        /// <param name="key">密文标识</param>
        /// <param name="plaintext">明文内容</param>
        /// <returns>返回密文</returns>
        string GetCiphertext(string key, string plaintext);

        /// <summary>
        /// 密文转明文
        /// </summary>
        /// <param name="key">密文标识</param>
        /// <param name="ciphertext">密文内容</param>
        /// <returns>返回明文</returns>
        string GetPlaintext(string key, string ciphertext);
    }
}