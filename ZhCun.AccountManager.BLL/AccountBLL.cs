﻿/**************************************************************************
创建时间:	2020/5/19 15:34:04    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZhCun.AccountManager.DBModel;
using ZhCun.DbCore.Cores;
using ZhCun.Utils;
using ZhCun.Utils.Helpers;

namespace ZhCun.AccountManager.BLL
{
    public class AccountBLL : BLLBaseCrud<TAccount>
    {
        public override ApiResult Del(TAccount model)
        {
            if (model.LastTime != null)
            {
                return RetErr("账户已经使用不允许删除");
            }

            return base.Del(model);
        }

        protected override void SetQuery(QueryCondition<TAccount> query, string searchVal)
        {
            if (!searchVal.IsEmpty())
            {
                string likeStr = $"%{searchVal}%";
                query.WhereAnd(s =>
                    s.WEx_Like(s.AccountName, likeStr) ||
                    s.WEx_Like(s.AccountNameCode, likeStr)
                    );
            }
            query.OrderBy(s => s.AddTime);
        }

        protected override ApiResult<string> VerifyModel(TAccount model, bool isAdd)
        {
            if (model.AccountName.IsEmpty())
            {
                return RetErr(nameof(model.AccountName), "账户名称不能为空");
            }
            if (DB.QueryExist<TAccount>(s => s.AccountName == model.AccountName && s.Id != model.Id))
            {
                return RetErr(nameof(model.AccountName), "账户名称已存在");
            }

            return RetOK<string>(string.Empty, isAdd ? "新增账户成功" : "更新账户成功");
        }
    }
}