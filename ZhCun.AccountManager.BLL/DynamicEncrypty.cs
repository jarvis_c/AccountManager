﻿/**************************************************************************
创建时间:	2020/5/28 17:34:43    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：实现动态加解密的实现，用于连接字符串或者其它敏感配置，从App.Config 的 DynamicEncryptyType 配置项读取
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.Utils.Helpers;

namespace ZhCun.Utils
{
    /// <summary>
    /// 动态加解密实现
    /// </summary>
    public static class DynamicEncrypty
    {
        static DynamicEncrypty()
        {
            string fullType = ConfigHelper.GetAppSettings("DynamicEncryptyType");
            if (!fullType.IsEmpty())
            {
                EncryptyHelper = ReflectionHelper.CreateInstance<IStringDynamicEncrypty>(fullType);
            }
        }

        readonly static IStringDynamicEncrypty EncryptyHelper;

        /// <summary>
        /// 明问转密文
        /// </summary>
        /// <param name="key">密文标识</param>
        /// <param name="plaintext">明文内容</param>
        /// <returns>返回密文</returns>
        public static string GetCiphertext(string key, string plaintext)
        {
            if (EncryptyHelper == null) return plaintext;
            return EncryptyHelper.GetCiphertext(key, plaintext);
        }

        /// <summary>
        /// 密文转明文
        /// </summary>
        /// <param name="key">密文标识</param>
        /// <param name="ciphertext">密文内容</param>
        /// <returns>返回明文</returns>
        public static string GetPlaintext(string key, string ciphertext)
        {
            if (EncryptyHelper == null) return ciphertext;
            return EncryptyHelper.GetPlaintext(key, ciphertext);
        }
    }
}