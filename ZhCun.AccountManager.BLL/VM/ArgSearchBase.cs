﻿/**************************************************************************
创建时间:	2020/5/23 10:18:36    
作	  者:	张存
邮 	  箱:	zhangcunliang@126.com

Copyright (c) zhcun.cn

描	述：
记	录：
***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.AccountManager.BLL.VM
{
    public class ArgSearchBase
    {
        /// <summary>
        /// 当前页
        /// </summary>
        public int PageNo { set; get; }
        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { set; get; }
        /// <summary>
        /// 模糊搜索值
        /// </summary>
        public string SearchVal { set; get; }
        /// <summary>
        /// 输出总行数（分页）
        /// </summary>
        public int OutRowCount { set; get; }
        /// <summary>
        /// 日期类型,  0:记录时间, 1:发生时间
        /// </summary>
        public int DateType { set; get; }
    }
}